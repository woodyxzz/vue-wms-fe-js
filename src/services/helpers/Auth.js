export function getLocalUser() {
  const userStr = localStorage.getItem('user');
  if (!userStr) {
    return null;
  }
  return JSON.parse(userStr);
}
export function getLocalId() {
  const idStr = localStorage.getItem('id');

  if (!idStr) {
    return null;
  }
  return JSON.parse(idStr);
}
export function getLocalWare() {
  const wareStr = localStorage.getItem('ware');

  if (!wareStr) {
    return null;
  }
  return JSON.parse(wareStr);
}
export function getLocalComp() {
  const compStr = localStorage.getItem('comp');
  if (!compStr) {
    return null;
  }
  return JSON.parse(compStr);
}
export function getLocalMatCate() {
  const cateStr = localStorage.getItem('matCate');
  if (!cateStr) {
    return null;
  }
  return JSON.parse(cateStr);
}

export function getLocalName() {
  const nameStr = localStorage.getItem('name');
  if (!nameStr) {
    return null;
  }
  return JSON.parse(nameStr);
}

export function getLocalProId() {
  const proStr = localStorage.getItem('proId');
  if (!proStr) {
    return null;
  }
  return JSON.parse(proStr);
}

export function getLocalOrderNum() {
  const orderNum = localStorage.getItem('orderNum');
  if (!orderNum) {
    return null;
  }
  return JSON.parse(orderNum);
}

export function getLocalOrderId() {
  const orderId = localStorage.getItem('orderId');
  if (!orderId) {
    return null;
  }
  return JSON.parse(orderId);
}

export function getLocalAdminId() {
  const adminId = localStorage.getItem('adminId');
  if (!adminId) {
    return null;
  }
  return JSON.parse(adminId);
}

export function getLocalCompInfoId() {
  const compInfoId = localStorage.getItem('compInfoId');
  if (!compInfoId) {
    return null;
  }
  return JSON.parse(compInfoId);
}

export function getLocalRowComp() {
  const rowComp = localStorage.getItem('rowComp');
  if (!rowComp) {
    return null;
  }
  return JSON.parse(rowComp);
}
export function getLocalReport(){
  const reportstr = localStorage.getItem('report');
  if(!reportstr){
    return null;
  }
  return JSON.parse(reportstr);
}
