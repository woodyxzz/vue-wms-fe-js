import HTTP from './ApiService';

export default {
  getCompany() {
    return HTTP().get('comps/company');
  },
  getAdmin() {
    return HTTP().get('admins/admin');
  },
  editCompany() {
    return HTTP().post('comps/companye');
  },
  editCompanyStatus() {
    return HTTP().post('comps/companyes');
  },
  getCountCompany() {
    return HTTP().get('comps/ncompany');
  },
  getCountWaitCompany() {
    return HTTP().get('comps/ncompanyw');
  },
};
