import axios from "axios";
import { CONFIG } from "../configs";

export default () => {
  return axios.create({ baseURL: CONFIG.BASEAPI });
};