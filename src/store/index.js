import Vue from 'vue';
import Vuex from 'vuex';
import {
  getLocalUser,
  getLocalId,
  getLocalComp,
  getLocalWare,
  getLocalMatCate,
  getLocalName,
  getLocalProId,
  getLocalOrderNum,
  getLocalOrderId,
  getLocalAdminId,
  getLocalCompInfoId,
  getLocalRowComp,
  getLocalReport
} from '../services/helpers/Auth';

const user = getLocalUser();
const id = getLocalId();
const ware = getLocalWare();
const comp = getLocalComp();
const cateType = getLocalMatCate();
const name = getLocalName();
const proId = getLocalProId();
const orderNum = getLocalOrderNum();
const orderId = getLocalOrderId();
const adminId = getLocalAdminId();
const compInfoId = getLocalCompInfoId();
const rowComp = getLocalRowComp();
const report = getLocalReport();
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    currentUser: user,
    companyId: compInfoId,
    adminId: null,
    row: rowComp,
    currentId: id,
    currentWare: ware,
    currentCompa: comp,
    currentCompany: comp,
    currentMatCate: cateType,
    currentName: name,
    currentProId: proId,
    currentOrderNum: orderNum,
    currentOrderId: orderId,
    currentAdminId: adminId,
    currentReport: report,
  },
  getters: {
    currentLoading(state) {
      return state.loading;
    },
    currentUser(state) {
      return state.currentUser;
    },
    currentComp(state) {
      return state.companyId;
    },
    currentAdmin(state) {
      return state.adminId;
    },
    currentRow(state) {
      return state.row;
    },
    currentId(state) {
      return state.currentId;
    },
    currentWare(state) {
      return state.currentWare;
    },
    currentCompa(state) {
      return state.currentCompa;
    },
    currentCompany(state) {
      return state.currentCompany;
    },
    currentMatCate(state) {
      return state.currentMatCate;
    },
    currentName(state) {
      return state.currentName;
    },
    currentProId(state) {
      return state.currentProId;
    },
    currentOrderNum(state) {
      return state.currentOrderNum;
    },
    currentOrderId(state) {
      return state.currentOrderId;
    },
    currentAdminId(state) {
      return state.currentAdminId;
    },
    currentReport(state){
      return state.currentReport;
    },
  },
  mutations: {
    isLoading(state, payload) {
      state.loading = payload;
    },
    isLogin(state, payload) {
      state.currentUser = payload;
      localStorage.setItem('user', JSON.stringify(state.currentUser));
    },
    isLogin2(state, payload) {
      state.currentId = payload;
      localStorage.setItem('id', JSON.stringify(state.currentId));
    },
    isLogin3(state, payload) {
      state.currentCompa = payload;
      localStorage.setItem('comp', JSON.stringify(state.currentCompa));
    },
    isLogin4(state, payload) {
      state.currentName = payload;
      localStorage.setItem('name', JSON.stringify(state.currentName));
    },
    isLogin5(state, payload) {
      state.currentAdminId = payload;
      localStorage.setItem('adminId', JSON.stringify(state.currentAdminId));
    },
    isUsestore(state, payload) {
      state.currentWare = payload;
      localStorage.setItem('ware', JSON.stringify(state.currentWare));
    },
    isGetreport(state, payload){
      state.currentReport = payload
      localStorage.setItem("report" , JSON.stringify(state.currentReport));
    },
    // isLogin3(state, payload) {
    //   state.currentCompany = payload;
    //   localStorage.setItem("comp", JSON.stringify(payload));
    // },
    isLogout(state) {
      localStorage.removeItem('user');
      localStorage.removeItem('id');
      localStorage.removeItem('ware');
      localStorage.removeItem('comp');
      localStorage.removeItem('matCate');
      localStorage.removeItem('name');
      localStorage.removeItem('proId');
      localStorage.removeItem('orderNum');
      localStorage.removeItem('orderId');
      localStorage.removeItem('adminId');
      localStorage.removeItem('compInfoId');
      localStorage.removeItem('rowComp');
      localStorage.removeItem("report");
      state.currentUser = null;
      state.companyId = null;
      state.adminId = null;
      state.currentId = null;
      state.currentCompany = null;
      state.currentMatCate = null;
      state.currentName = null;
      state.currentProId = null;
      state.currentOrderNum = null;
      state.currentOrderId = null;
      state.currentAdminId = null;
      state.row = null;
      state.currentReport = null;
    },
    isCateType(state, payload) {
      state.currentMatCate = payload;
      localStorage.setItem('matCate', JSON.stringify(payload));
    },
    isProId(state, payload) {
      state.currentProId = payload;
      localStorage.setItem('proId', JSON.stringify(payload));
    },
    isOrderNum(state, payload) {
      state.currentOrderNum = payload;
      localStorage.setItem('orderNum', JSON.stringify(payload));
    },
    isOrderId(state, payload) {
      state.currentOrderId = payload;
      localStorage.setItem('orderId', JSON.stringify(payload));
      
    },
    setComp(state, payload) {
      state.companyId = payload;
      localStorage.setItem('compInfoId', JSON.stringify(payload));
    },
    setAdmin(state, payload) {
      state.adminId = payload;
    },
    setRow(state, payload) {
      state.row = payload;
      localStorage.setItem('rowComp', JSON.stringify(payload));
    },
  },
  actions: {
    login(context) {
      context.commit('login');
    },
  },
  modules: {},
});
